# aws-lambda-cmake

This is a template (seed project) for AWS lambdas in C++. It is an implementation of the nice and easy AWS Lambda 
demo for C++ from https://aws.amazon.com/blogs/compute/introducing-the-c-lambda-runtime/ .
I did not like the idea that the lambda runtime library must be downloaded and installed seperately (or hard-included
into the project), so I started playing around with creating a demo in which CMake fetches, builds and provides the 
aws-lambda-runtime automagically.


## Usage

Configure CMake:
- cmake .
Build the project (this step also pulls the aws-lambda-runtime from Github and builds it)
- make
Create the deployable ZIP (note that make has TAB-Complete and will show you possible make targets)
- make aws-lambda-package-CppLambda

The created ZIP-file can now be deployed to AWS as a lambda. See the src/main.cpp to see what it
actually does and how it does it.


## Deploying to AWS

The AWS Lambda demo by Amazon (link: see above) explains how to create a run policy and how to deploy your lambda function
using the AWS CLI. If you are familiar with AWS, this is probably the go-to way. I personally just used the browser GUI of
AWS Lambda to upload the ZIP (possibly via an S3 bucket), set the run policies and test it.
