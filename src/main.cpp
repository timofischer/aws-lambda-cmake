#include <aws/lambda-runtime/runtime.h>
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

using namespace aws::lambda_runtime;
using namespace rapidjson;

invocation_response my_handler(invocation_request const& request) {
    Document requestJson;
    requestJson.Parse(request.payload.c_str());

    Document replyJson;
    replyJson.SetObject();
    auto& allocator = replyJson.GetAllocator();
    Value squares(kArrayType);

    for (const auto& entry : requestJson["values"].GetArray()) {
        const double value = entry.GetDouble();
        Value square;
        square.SetDouble(value * value);
        squares.PushBack(square, allocator);
    }
    replyJson.AddMember("values", squares, allocator);

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    replyJson.Accept(writer);
    const std::string replyString = buffer.GetString();

    return invocation_response::success(replyString, "application/json");


    // rapidjson::Document jsonDoc;
    // jsonDoc.SetObject();
    // rapidjson::Value myArray(rapidjson::kArrayType);
    // rapidjson::Document::AllocatorType& allocator = jsonDoc.GetAllocator();

    // std::vector<Roster*>::iterator iter = roster.begin();
    // std::vector<Roster*>::iterator eiter = roster.end();
    // for (; iter != eiter; ++iter)
    // {
    //     rapidjson::Value objValue;
    //     objValue.SetObject();
    //     objValue.AddMember("playername", (*iter)->PlayerName().c_str(), allocator);
    //     objValue.AddMember("position", (*iter)->Position().c_str(), allocator);

    //     myArray.PushBack(objValue, allocator);
    // }

    // jsonDoc.AddMember("array", myArray, allocator);
    // rapidjson::StringBuffer strbuf;
    // rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    // jsonDoc.Accept(writer);

    // const char *jsonString = strbuf.GetString();  // to examine the encoding...
}

int main() {
    run_handler(my_handler);
    return 0;
}
