project(CppLambda LANGUAGES CXX)
cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_STANDARD 17)


add_subdirectory(thirdparty)
list(APPEND CMAKE_PREFIX_PATH ${THIRDPARTY_PREFIX_PATH})


find_package(aws-lambda-runtime QUIET)

if(NOT ${aws-lambda-runtime_FOUND})
    # rerun cmake in initial build
    # will update cmakecache/project files on first build
    # so you may have to reload project after first build    
    add_custom_target(thirdparty ${CMAKE_COMMAND} ${CMAKE_SOURCE_DIR} DEPENDS aws-lambda-runtime)
else()
    # Thirdparty becomes a dummy target after first build
    # this prevents cmake from rebuilding cache/projects on subsequent builds    
    add_custom_target(thirdparty)
endif()

message("THIRDPARTY_PREFIX_PATH=" ${THIRDPARTY_PREFIX_PATH})

add_subdirectory(src)
